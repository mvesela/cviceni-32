export default class Component {
  constructor($element) {
    this.target = $element;
    this.$slide = this.target.closest('.slide');
    this.lang = document.documentElement.getAttribute('lang');

    if (!HISTORYLAB.import.done) {

      if (this.$slide) {
        this.$buttonFeedback = this.$slide.querySelector('[data-feedback-button]');

        if (this.$buttonFeedback) {
          this.$buttonFeedbackText = this.$buttonFeedback.querySelector('span');
        }
      }


      this.showFeedback = this.showFeedback.bind(this);
    }
  }

  showFeedback() {
    if (this.$buttonFeedback && this.$buttonFeedback.classList.contains('button-hidden')) {
      this.$buttonFeedback.classList.remove('button-hidden');
    }
  }

  hideFeedback() {
    this.$buttonFeedback.classList.add('button-hidden');
  }
}
